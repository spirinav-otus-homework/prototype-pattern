﻿namespace PrototypePatternCore.Interfaces
{
	public interface IMyCloneable<T>
	{
		T GetDeepClone();
	}
}