﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using PrototypePatternCore.Interfaces;

namespace PrototypePatternCore.Models
{
	/// <summary>
	/// Сооружение
	/// </summary>
	public class Construction : ImmovableFixedAsset, IMyCloneable<Construction>
	{
		/// <summary>
		/// Площадь
		/// </summary>
		public int FloorSpace { get; set; }

		/// <summary>
		/// Компоненты основного средства
		/// </summary>
		public IEnumerable<string> Components { get; set; }

		public Construction()
		{
		}

		public Construction(Construction construction) : base(construction)
		{
			FloorSpace = construction.FloorSpace;
			Components = construction.Components?.Select(x => x).ToArray();
		}

		public override Construction GetDeepClone()
		{
			return new Construction(this);
		}

		[SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
		public override int GetHashCode()
		{
			var components = Components != null ? string.Join("", Components) : "";
			return HashCode.Combine(base.GetHashCode(), FloorSpace, components);
		}

		public override bool Equals(object obj)
		{
			if (obj is not Construction construction)
			{
				return false;
			}

			return GetHashCode() == construction.GetHashCode();
		}
	}
}