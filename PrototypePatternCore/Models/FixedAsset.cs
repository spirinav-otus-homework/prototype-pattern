﻿using System;
using System.Diagnostics.CodeAnalysis;

using PrototypePatternCore.Interfaces;

namespace PrototypePatternCore.Models
{
	/// <summary>
	/// Основное средство
	/// </summary>
	public class FixedAsset : IMyCloneable<FixedAsset>, ICloneable
	{
		/// <summary>
		/// Инвентарный номер
		/// </summary>
		public string AssetNumber { get; set; }

		/// <summary>
		/// Название
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Дата принятия к учету
		/// </summary>
		public DateTime RegistrationDate { get; set; }

		/// <summary>
		/// Дата снятия с учета
		/// </summary>
		public DateTime DeregistrationDate { get; set; }

		public FixedAsset()
		{
		}

		public FixedAsset(FixedAsset fixedAsset)
		{
			AssetNumber = fixedAsset.AssetNumber;
			Name = fixedAsset.Name;
			RegistrationDate = fixedAsset.RegistrationDate;
			DeregistrationDate = fixedAsset.DeregistrationDate;
		}

		public virtual FixedAsset GetDeepClone()
		{
			return new FixedAsset(this);
		}

		public object Clone()
		{
			return GetDeepClone();
		}

		[SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
		public override int GetHashCode()
		{
			return HashCode.Combine(AssetNumber, Name, RegistrationDate, DeregistrationDate);
		}

		public override bool Equals(object obj)
		{
			if (obj is not FixedAsset fixedAsset)
			{
				return false;
			}

			return GetHashCode() == fixedAsset.GetHashCode();
		}
	}
}