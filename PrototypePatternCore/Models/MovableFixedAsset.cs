﻿using System;
using System.Diagnostics.CodeAnalysis;

using PrototypePatternCore.Interfaces;

namespace PrototypePatternCore.Models
{
	/// <summary>
	/// Движимое основное средство
	/// </summary>
	public class MovableFixedAsset : FixedAsset, IMyCloneable<MovableFixedAsset>
	{
		/// <summary>
		/// Признак обязательности государственной регистрации
		/// </summary>
		public bool IsRequiredOfficialRegistration { get; set; }

		public MovableFixedAsset()
		{
		}

		public MovableFixedAsset(MovableFixedAsset movableFixedAsset) : base(movableFixedAsset)
		{
			IsRequiredOfficialRegistration = movableFixedAsset.IsRequiredOfficialRegistration;
		}

		public override MovableFixedAsset GetDeepClone()
		{
			return new MovableFixedAsset(this);
		}

		[SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
		public override int GetHashCode()
		{
			return HashCode.Combine(base.GetHashCode(), IsRequiredOfficialRegistration);
		}

		public override bool Equals(object obj)
		{
			if (obj is not MovableFixedAsset movableFixedAsset)
			{
				return false;
			}

			return GetHashCode() == movableFixedAsset.GetHashCode();
		}
	}
}