﻿using System;
using System.Diagnostics.CodeAnalysis;

using PrototypePatternCore.Interfaces;

namespace PrototypePatternCore.Models
{
	/// <summary>
	/// Недвижимое основное средство
	/// </summary>
	public class ImmovableFixedAsset : FixedAsset, IMyCloneable<ImmovableFixedAsset>
	{
		/// <summary>
		/// Кадастровый номер
		/// </summary>
		public string CadastralNumber { get; set; }

		/// <summary>
		/// Адрес
		/// </summary>
		public string Address { get; set; }

		public ImmovableFixedAsset()
		{
		}

		public ImmovableFixedAsset(ImmovableFixedAsset immovableFixedAsset) : base(immovableFixedAsset)
		{
			CadastralNumber = immovableFixedAsset.CadastralNumber;
			Address = immovableFixedAsset.Address;
		}

		public override ImmovableFixedAsset GetDeepClone()
		{
			return new ImmovableFixedAsset(this);
		}

		[SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
		public override int GetHashCode()
		{
			return HashCode.Combine(base.GetHashCode(), CadastralNumber, Address);
		}

		public override bool Equals(object obj)
		{
			if (obj is not ImmovableFixedAsset immovableFixedAsset)
			{
				return false;
			}

			return GetHashCode() == immovableFixedAsset.GetHashCode();
		}
	}
}