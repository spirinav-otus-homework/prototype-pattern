using System;
using System.Collections.Generic;

using PrototypePatternCore.Models;

using Xunit;

namespace PrototypePatternCore.Test
{
	public class ConstructionTest
	{
		private static Construction GetConstruction()
		{
			return new()
			{
				AssetNumber = "100100100",
				Name = "���������������� ���������� �1",
				RegistrationDate = new DateTime(1990, 1, 1),
				DeregistrationDate = new DateTime(2020, 12, 31),
				CadastralNumber = "31:XX:XXXXXXX:X",
				Address = "���-�� �� ��������� ��",
				FloorSpace = 10000,
				Components = new List<string>()
				{
					"������",
					"��������� ���������",
					"�������� ���������"
				}
			};
		}

		[Fact]
		public void GetDeepClone_ShouldReturnAreNotTheSameInstance()
		{
			// Arrange
			var construction = GetConstruction();

			// Act
			var result = construction.GetDeepClone();

			// Assert
			Assert.NotSame(result, construction);
		}

		[Fact]
		public void Clone_ShouldReturnAreNotTheSameInstance()
		{
			// Arrange
			var construction = GetConstruction();

			// Act
			var result = construction.Clone();

			// Assert
			Assert.NotSame(result, construction);
		}

		[Fact]
		public void GetDeepClone_ShouldReturnReallyDeepClone()
		{
			// Arrange
			var construction = GetConstruction();

			// Act
			var result = construction.GetDeepClone();

			// Assert
			Assert.NotSame(result.Components, construction.Components);
		}

		[Fact]
		public void GetDeepClone_ShouldReturnResultWithTheSameFieldValues()
		{
			// Arrange
			var construction = GetConstruction();

			// Act
			var result = construction.GetDeepClone();

			// Assert
			Assert.Equal(result, construction);
		}

		[Fact]
		public void Clone_ShouldReturnResultWithTheSameFieldValues()
		{
			// Arrange
			var construction = GetConstruction();

			// Act
			var result = construction.Clone();

			// Assert
			Assert.Equal(result, construction);
		}

		[Fact]
		public void GetDeepClone_ShouldReturnResultWhoseModifyDoesNotChangeOriginalObject()
		{
			// Arrange
			var construction = GetConstruction();

			// Act
			var result = construction.GetDeepClone();
			result.FloorSpace = 500;
			result.Components = null;

			// Assert
			Assert.NotEqual(result, construction);
		}

		[Fact]
		public void Clone_ShouldReturnResultWhoseModifyDoesNotChangeOriginalObject()
		{
			// Arrange
			var construction = GetConstruction();

			// Act
			var result = (Construction) construction.Clone();
			result.FloorSpace = 500;
			result.Components = null;

			// Assert
			Assert.NotEqual(result, construction);
		}

		[Fact]
		public void Clone_ShouldReturnResultEqualGetDeepCloneResult()
		{
			// Arrange
			var construction = GetConstruction();

			// Act
			var resultClone = (Construction) construction.Clone();
			var resultGetDeepClone = construction.GetDeepClone();

			// Assert
			Assert.Equal(resultClone, resultGetDeepClone);
		}
	}
}