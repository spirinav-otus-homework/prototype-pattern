using System;

using PrototypePatternCore.Models;

using Xunit;

namespace PrototypePatternCore.Test
{
	public class ImmovableFixedAssetTest
	{
		private static ImmovableFixedAsset GetImmovableFixedAsset()
		{
			return new()
			{
				AssetNumber = "100100100",
				Name = "���������������� ���������� �1",
				RegistrationDate = new DateTime(1990, 1, 1),
				DeregistrationDate = new DateTime(2020, 12, 31),
				CadastralNumber = "31:XX:XXXXXXX:X",
				Address = "���-�� �� ��������� ��"
			};
		}

		[Fact]
		public void GetDeepClone_ShouldReturnAreNotTheSameInstance()
		{
			// Arrange
			var immovableFixedAsset = GetImmovableFixedAsset();

			// Act
			var result = immovableFixedAsset.GetDeepClone();

			// Assert
			Assert.NotSame(result, immovableFixedAsset);
		}

		[Fact]
		public void Clone_ShouldReturnAreNotTheSameInstance()
		{
			// Arrange
			var immovableFixedAsset = GetImmovableFixedAsset();

			// Act
			var result = immovableFixedAsset.Clone();

			// Assert
			Assert.NotSame(result, immovableFixedAsset);
		}

		[Fact]
		public void GetDeepClone_ShouldReturnResultWithTheSameFieldValues()
		{
			// Arrange
			var immovableFixedAsset = GetImmovableFixedAsset();

			// Act
			var result = immovableFixedAsset.GetDeepClone();

			// Assert
			Assert.Equal(result, immovableFixedAsset);
		}

		[Fact]
		public void Clone_ShouldReturnResultWithTheSameFieldValues()
		{
			// Arrange
			var immovableFixedAsset = GetImmovableFixedAsset();

			// Act
			var result = immovableFixedAsset.Clone();

			// Assert
			Assert.Equal(result, immovableFixedAsset);
		}

		[Fact]
		public void GetDeepClone_ShouldReturnResultWhoseModifyDoesNotChangeOriginalObject()
		{
			// Arrange
			var immovableFixedAsset = GetImmovableFixedAsset();

			// Act
			var result = immovableFixedAsset.GetDeepClone();
			result.CadastralNumber = "31:99:9999999:9";
			result.Address = "���-�� ���";

			// Assert
			Assert.NotEqual(result, immovableFixedAsset);
		}

		[Fact]
		public void Clone_ShouldReturnResultWhoseModifyDoesNotChangeOriginalObject()
		{
			// Arrange
			var immovableFixedAsset = GetImmovableFixedAsset();

			// Act
			var result = (ImmovableFixedAsset) immovableFixedAsset.Clone();
			result.CadastralNumber = "31:99:9999999:9";
			result.Address = "���-�� ���";

			// Assert
			Assert.NotEqual(result, immovableFixedAsset);
		}

		[Fact]
		public void Clone_ShouldReturnResultEqualGetDeepCloneResult()
		{
			// Arrange
			var immovableFixedAsset = GetImmovableFixedAsset();

			// Act
			var resultClone = (ImmovableFixedAsset) immovableFixedAsset.Clone();
			var resultGetDeepClone = immovableFixedAsset.GetDeepClone();

			// Assert
			Assert.Equal(resultClone, resultGetDeepClone);
		}
	}
}