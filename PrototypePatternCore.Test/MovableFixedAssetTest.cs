using System;

using PrototypePatternCore.Models;

using Xunit;

namespace PrototypePatternCore.Test
{
	public class MovableFixedAssetTest
	{
		private static MovableFixedAsset GetMovableFixedAsset()
		{
			return new()
			{
				AssetNumber = "100100100",
				Name = "���������������� ���������� �1",
				RegistrationDate = new DateTime(1990, 1, 1),
				DeregistrationDate = new DateTime(2020, 12, 31),
				IsRequiredOfficialRegistration = true
			};
		}

		[Fact]
		public void GetDeepClone_ShouldReturnAreNotTheSameInstance()
		{
			// Arrange
			var movableFixedAsset = GetMovableFixedAsset();

			// Act
			var result = movableFixedAsset.GetDeepClone();

			// Assert
			Assert.NotSame(result, movableFixedAsset);
		}

		[Fact]
		public void Clone_ShouldReturnAreNotTheSameInstance()
		{
			// Arrange
			var movableFixedAsset = GetMovableFixedAsset();

			// Act
			var result = movableFixedAsset.Clone();

			// Assert
			Assert.NotSame(result, movableFixedAsset);
		}

		[Fact]
		public void GetDeepClone_ShouldReturnResultWithTheSameFieldValues()
		{
			// Arrange
			var movableFixedAsset = GetMovableFixedAsset();

			// Act
			var result = movableFixedAsset.GetDeepClone();

			// Assert
			Assert.Equal(result, movableFixedAsset);
		}

		[Fact]
		public void Clone_ShouldReturnResultWithTheSameFieldValues()
		{
			// Arrange
			var movableFixedAsset = GetMovableFixedAsset();

			// Act
			var result = movableFixedAsset.Clone();

			// Assert
			Assert.Equal(result, movableFixedAsset);
		}

		[Fact]
		public void GetDeepClone_ShouldReturnResultWhoseModifyDoesNotChangeOriginalObject()
		{
			// Arrange
			var movableFixedAsset = GetMovableFixedAsset();

			// Act
			var result = movableFixedAsset.GetDeepClone();
			result.IsRequiredOfficialRegistration = false;

			// Assert
			Assert.NotEqual(result, movableFixedAsset);
		}

		[Fact]
		public void Clone_ShouldReturnResultWhoseModifyDoesNotChangeOriginalObject()
		{
			// Arrange
			var movableFixedAsset = GetMovableFixedAsset();

			// Act
			var result = (MovableFixedAsset) movableFixedAsset.Clone();
			result.IsRequiredOfficialRegistration = false;

			// Assert
			Assert.NotEqual(result, movableFixedAsset);
		}

		[Fact]
		public void Clone_ShouldReturnResultEqualGetDeepCloneResult()
		{
			// Arrange
			var movableFixedAsset = GetMovableFixedAsset();

			// Act
			var resultClone = (MovableFixedAsset) movableFixedAsset.Clone();
			var resultGetDeepClone = movableFixedAsset.GetDeepClone();

			// Assert
			Assert.Equal(resultClone, resultGetDeepClone);
		}
	}
}