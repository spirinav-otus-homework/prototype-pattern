using System;

using PrototypePatternCore.Models;

using Xunit;

namespace PrototypePatternCore.Test
{
	public class FixedAssetTest
	{
		private static FixedAsset GetFixedAsset()
		{
			return new()
			{
				AssetNumber = "100100100",
				Name = "���������������� ���������� �1",
				RegistrationDate = new DateTime(1990, 1, 1),
				DeregistrationDate = new DateTime(2020, 12, 31)
			};
		}

		[Fact]
		public void GetDeepClone_ShouldReturnAreNotTheSameInstance()
		{
			// Arrange
			var fixedAsset = GetFixedAsset();

			// Act
			var result = fixedAsset.GetDeepClone();

			// Assert
			Assert.NotSame(result, fixedAsset);
		}

		[Fact]
		public void Clone_ShouldReturnAreNotTheSameInstance()
		{
			// Arrange
			var fixedAsset = GetFixedAsset();

			// Act
			var result = fixedAsset.Clone();

			// Assert
			Assert.NotSame(result, fixedAsset);
		}

		[Fact]
		public void GetDeepClone_ShouldReturnResultWithTheSameFieldValues()
		{
			// Arrange
			var fixedAsset = GetFixedAsset();

			// Act
			var result = fixedAsset.GetDeepClone();

			// Assert
			Assert.Equal(result, fixedAsset);
		}

		[Fact]
		public void Clone_ShouldReturnResultWithTheSameFieldValues()
		{
			// Arrange
			var fixedAsset = GetFixedAsset();

			// Act
			var result = fixedAsset.Clone();

			// Assert
			Assert.Equal(result, fixedAsset);
		}

		[Fact]
		public void GetDeepClone_ShouldReturnResultWhoseModifyDoesNotChangeOriginalObject()
		{
			// Arrange
			var fixedAsset = GetFixedAsset();

			// Act
			var result = fixedAsset.GetDeepClone();
			result.Name = "��� �1";

			// Assert
			Assert.NotEqual(result.Name, fixedAsset.Name);
		}

		[Fact]
		public void Clone_ShouldReturnResultWhoseModifyDoesNotChangeOriginalObject()
		{
			// Arrange
			var fixedAsset = GetFixedAsset();

			// Act
			var result = (FixedAsset) fixedAsset.Clone();
			result.Name = "��� �1";

			// Assert
			Assert.NotEqual(result.Name, fixedAsset.Name);
		}

		[Fact]
		public void Clone_ShouldReturnResultEqualGetDeepCloneResult()
		{
			// Arrange
			var fixedAsset = GetFixedAsset();

			// Act
			var resultClone = (FixedAsset) fixedAsset.Clone();
			var resultGetDeepClone = fixedAsset.GetDeepClone();

			// Assert
			Assert.Equal(resultClone, resultGetDeepClone);
		}
	}
}